import React from "react";
import "./Alert.css";
const Alert = props => {
    const alertType = `${props.type} alert`;


    return (<div className={alertType} style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'
    }}>
        <i className="close" onClick={props.dismiss} style={{
             // display: props.dismiss===undefined? 'block' : 'none'
        }}>X</i>
        {props.children}

    </div>);
};
export default Alert;