import React, {Component} from "react";
import "./App.css";
import Alert from "./components/UI/Alert";

class App extends Component {
    state = {
        show: true,
        components: [
            {type: 'warning', show: true, handler: this.someHandler, id: '1'},
            {type: 'primary', show: true, handler: ()=>console.log("asd"), id: '2'},
            {type: 'success', show: true, handler: this.someHandler, id: '3'},
            {type: 'danger', show: true, handler: this.someHandler, id: '4'}
        ]
    };

    someHandler = () => {
    console.log("das");
};

    render() {
        return (
            this.state.components.map(function (element) {
                return (<Alert
                    type={element.type}
                    dismiss={element.handler}
                    show={element.show}
                    key={element.id}

                >This is a warning type alert</Alert>);
            })
        );
    }
}

export default App;
